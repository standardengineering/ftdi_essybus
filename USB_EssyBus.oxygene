﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <ProductVersion>3.5</ProductVersion>
    <RootNamespace>Essy.EssyBus.PhysicalLayer</RootNamespace>
    <OutputType>Library</OutputType>
    <AssemblyName>USB_EssyBus</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <Configuration Condition="'$(Configuration)' == ''">Debug</Configuration>
    <Platform Condition="'$(Platform)' == ''">AnyCPU</Platform>
    <TargetFrameworkVersion>v4.8</TargetFrameworkVersion>
    <ProjectGuid>{C808EBDF-D46E-4AD1-9A54-8D508C6F3A69}</ProjectGuid>
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
    <ProjectView>ShowAllFiles</ProjectView>
    <Mode>Echoes</Mode>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Debug'">
    <OutputPath>.\bin\Debug\</OutputPath>
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <GeneratePDB>True</GeneratePDB>
    <Optimize>False</Optimize>
    <CpuType>anycpu</CpuType>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Release'">
    <OutputPath>.\bin\Release\</OutputPath>
    <EnableAsserts>False</EnableAsserts>
    <CpuType>anycpu</CpuType>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="mscorlib" />
    <Reference Include="RemObjects.Elements.Cirrus">
      <HintPath>C:\Program Files (x86)\RemObjects Software\Elements\Echoes\Reference Assemblies\RemObjects.Elements.Cirrus.dll</HintPath>
    </Reference>
    <Reference Include="System" />
    <Reference Include="System.Core" />
    <ProjectReference Include="..\essybus\EssyBus.oxygene">
      <Name>EssyBus</Name>
      <Project>{f45ce426-5d40-4378-8024-cc2a23b9d619}</Project>
      <Private>True</Private>
      <HintPath>..\essybus\bin\Debug\EssyBus.dll</HintPath>
    </ProjectReference>
    <ProjectReference Include="..\oxygeneaspects\trunk\Source\Prism.StandardAspects\Prism.StandardAspects.oxygene">
      <Name>Prism.StandardAspects</Name>
      <Project>{fdfa9e51-2441-4785-9599-44a17d1b72c7}</Project>
      <Private>True</Private>
      <HintPath>..\oxygeneaspects\trunk\Source\Prism.StandardAspects\bin\Debug\Prism.StandardAspects.dll</HintPath>
    </ProjectReference>
    <Reference Include="FTDIWrapper">
      <HintPath>N:\Essy.FTDIWrapper.6.2.0\lib\net46\FTDIWrapper.dll</HintPath>
      <Private>True</Private>
    </Reference>
  </ItemGroup>
  <ItemGroup>
    <Compile Include="Exceptions.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <EmbeddedResource Include="Properties\Resources.cs.resx" />
    <EmbeddedResource Include="Properties\Resources.de.resx" />
    <EmbeddedResource Include="Properties\Resources.es.resx" />
    <EmbeddedResource Include="Properties\Resources.fr.resx" />
    <EmbeddedResource Include="Properties\Resources.it.resx" />
    <EmbeddedResource Include="Properties\Resources.pt-BR.resx" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
    <Compile Include="USB_FTDI_RS485.pas" />
    <Content Include="packages.config" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.Legacy.targets" />
</Project>