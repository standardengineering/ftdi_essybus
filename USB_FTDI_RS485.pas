﻿//Copyright (C) 2009 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.EssyBus.PhysicalLayer;

interface

uses
  Prism.StandardAspects,
  System.Threading,
  Essy.EssyBus,
  Essy.FTDIWrapper;

type
  USB_FTDI_RS485 = public class(IPhysicalLayer)
  private
    fFTDIDevice: FTDI_Device;
    fWaitObject: EventWaitHandle := new EventWaitHandle(True, EventResetMode.ManualReset);
    fAbortWaitForPinChange: Boolean := False;
    method fDoWaitForPinChange; async;
  protected
  public
    method ReadFromEEProm(aSize: UInt32): array of Byte;
    method WriteToEEProm(aData: array of Byte);
    method WriteBytes(aData: array of System.Byte);
    method ReadBytes(aNumberOfBytes: Byte): array of System.Byte;
    method GetInterruptLineStatus: Boolean;
    method ResetInterface: Boolean;
    method ClearBuffers;
    [aspect:Disposable]
    method Dispose; 
    method GetUSBDriverVersion: Version;
    method SetupDevice;
    constructor(aDeviceInfo: FTDI_DeviceInfo);
    event ThreadException: ThreadExceptionEventHandler;
    event InterruptLineChange: Essy.EssyBus.PhysicalLayerInterruptEventHandler;
    property FTDIDevice: FTDI_Device read fFTDIDevice write fFTDIDevice;
  end;
  
implementation

method USB_FTDI_RS485.ReadBytes(aNumberOfBytes: Byte): array of System.Byte;
begin  
  try
    result := self.fFTDIDevice.ReadBytes(aNumberOfBytes);
  except
    on ex: FTDIDeviceException do
    begin
      raise new USB_FTDI_RS485_Exception(Essy.EssyBus.PhysicalLayer.Properties.Resources.strRead_Bytes_failed_ + Environment.NewLine + Essy.EssyBus.PhysicalLayer.Properties.Resources.strCheck_to_make_sure_the_USB_port_and_cable_are_in_making_a_good_connection_, ex);
    end;
  end;
end;

method USB_FTDI_RS485.WriteBytes(aData: array of System.Byte);
begin
  try
    self.fFTDIDevice.WriteBytes(aData);
  except
    on ex: FTDIDeviceException do
    begin
      raise new USB_FTDI_RS485_Exception(Essy.EssyBus.PhysicalLayer.Properties.Resources.strWrite_data_failed_ + Environment.NewLine + Essy.EssyBus.PhysicalLayer.Properties.Resources.strCheck_to_make_sure_the_USB_port_and_cable_are_in_making_a_good_connection_, ex);
    end;
  end; 
end;

constructor USB_FTDI_RS485(aDeviceInfo: FTDI_DeviceInfo);
begin
  self.fFTDIDevice := new FTDI_Device(aDeviceInfo);
  var pid := self.fFTDIDevice.GetDeviceProductID();
  if pid = $8C90 then self.fFTDIDevice.SetDeviceProductID($6010); //if using old custom PID revert to default PID
  self.SetupDevice();
end;

method USB_FTDI_RS485.fDoWaitForPinChange;
begin
  fWaitObject.Reset();
  fAbortWaitForPinChange := false;
  loop
  begin
    try
      var intState: Boolean := (StatusPins.CTS in self.fFTDIDevice.GetStatusPins);
      if intState then // when line went low
      begin
        self.InterruptLineChange:Invoke(self); //if not nil then invoke delegate
      end;
      loop
      begin
        self.fFTDIDevice.WaitForPinChange(Timeout.Infinite);
        if fAbortWaitForPinChange then 
        begin
          fWaitObject.Set();
          exit;
        end;
        intState := (StatusPins.CTS in self.fFTDIDevice.GetStatusPins);
        while intState do // when line went low
        begin
          self.InterruptLineChange:Invoke(self); //if not nil then invoke delegate
          intState := (StatusPins.CTS in self.fFTDIDevice.GetStatusPins);
        end;
      end;
    except
      on ex: Exception do //catch all exceptions and send them to the ThreadException event so they can be handled by the main thread
      begin
        self.ThreadException:Invoke(self, new ThreadExceptionEventArgs(ex)); 
        Thread.Sleep(100);
      end;
    end;
  end;
end;

method USB_FTDI_RS485.GetInterruptLineStatus: Boolean;
begin
  result := (StatusPins.CTS in self.fFTDIDevice.GetStatusPins);
end;

method USB_FTDI_RS485.Dispose;
begin
  self.fAbortWaitForPinChange := True;
  self.fFTDIDevice.SignalPinChangeEvent();
  self.fWaitObject.WaitOne();
  self.fFTDIDevice.DisableWaitForPinChangeEvent;
  self.fFTDIDevice.Dispose(); //clean up internal FTDI device
end;

method USB_FTDI_RS485.SetupDevice;
begin
  self.fFTDIDevice.SetBaudrate(50000); //50 KBit
  self.fFTDIDevice.SetParameters(DataLength.EightBits, Parity.None, StopBits.OneStopBit);
  self.fFTDIDevice.SetTimeOuts(2000, 2000);
  self.fFTDIDevice.ClearReceiveBuffer;
  self.fFTDIDevice.ClearSendBuffer;
  self.fFTDIDevice.SetBufferFlushTimer(2); 
  self.fFTDIDevice.SetResetPipeRetryCount(1000); //1000 retries before USB error 
  self.fFTDIDevice.EnableWaitForPinChangeEvent();
  self.fDoWaitForPinChange(); //start interrupt thread  
end;

method USB_FTDI_RS485.ResetInterface: Boolean;
begin
  var retryCounter := 1;
  while not self.fFTDIDevice.TryReloadDevice do
  begin 
    if retryCounter > 10 then exit false;
    inc(retryCounter);
  end;
  self.SetupDevice();
  result := true;
end;

method USB_FTDI_RS485.ClearBuffers;
begin
  try
    self.fFTDIDevice.ClearReceiveBuffer();
    self.fFTDIDevice.ClearSendBuffer();
  except
    on ex: FTDIDeviceException do
    begin
      raise new USB_FTDI_RS485_Exception(Essy.EssyBus.PhysicalLayer.Properties.Resources.strClear_Buffers_Failed + Environment.NewLine + Essy.EssyBus.PhysicalLayer.Properties.Resources.strCheck_to_make_sure_the_USB_port_and_cable_are_in_making_a_good_connection_, ex);
    end;
  end;
end;

method USB_FTDI_RS485.GetUSBDriverVersion: Version;
begin
  result := self.fFTDIDevice.GetDriverVersion();
end;

method USB_FTDI_RS485.ReadFromEEProm(aSize: UInt32): array of Byte;
begin
  result := self.fFTDIDevice.ReadBinaryFromUAEEProm(aSize);
end;

method USB_FTDI_RS485.WriteToEEProm(aData: array of Byte);
begin
  self.fFTDIDevice.WriteToUAEEProm(aData);
end;

end.